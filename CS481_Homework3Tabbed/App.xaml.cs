﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CS481_Homework3Tabbed.Services;
using CS481_Homework3Tabbed.Views;

namespace CS481_Homework3Tabbed
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
