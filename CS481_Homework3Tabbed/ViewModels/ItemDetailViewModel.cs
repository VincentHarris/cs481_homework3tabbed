﻿using System;

using CS481_Homework3Tabbed.Models;

namespace CS481_Homework3Tabbed.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.Text;
            Item = item;
        }
    }
}
